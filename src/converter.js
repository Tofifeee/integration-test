/**
* Padding outputs 2 characters always
* @param {string} hex one or two characters
* @returns {string} hex with two characters
*/
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    /** 
    * Converts RGB to Hex string
    * @param {number} red 0-255
    * @param {number} green 0-255
    * @param {number} blue 0-255
    * @returns {string} hex value
    */

    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // 0-255 -> 0-ff
        const greenHex = green.toString(16); // 0-255 -> 0-ff
        const blueHex = blue.toString(16); // 0-255 -> 0-ff


        return pad(redHex) + pad(greenHex) + pad(blueHex); // hex string 6 characters
    },

    /**
     * @param {string} hex hex string of a color
     * @returns {string} rgb string
     */
    hexToRgb: (hex) => {
        const biginit = parseInt(hex, 16);
        const red = (biginit >> 16) & 255;
        const green = (biginit >> 8) & 255;
        const blue = biginit & 255;

        return red + "," + green + "," + blue;
    }
}