// TDD - Unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic color", () => {
            const redHex = converter.rgbToHex(255,0,0); // red ff0000
            const greenHex = converter.rgbToHex(0,255,0); // green 00ff00
            const blueHex = converter.rgbToHex(0,0,255); // blue 0000ff

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    });

    describe("Hex to RGB conversion", () => {
        it("converts the basic color", () => {
            const red = converter.hexToRgb('ff0000'); // 25500
            const green = converter.hexToRgb('00ff00'); // 02550
            const blue = converter.hexToRgb('0000ff'); // 00255

            expect(red).to.equal('255,0,0');
            expect(green).to.equal('0,255,0');
            expect(blue).to.equal('0,0,255');
        });
    });
});